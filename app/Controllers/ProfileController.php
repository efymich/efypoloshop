<?php


namespace app\Controllers;


class ProfileController
{
    public function ShowData(): array
    {
        if (!$_SESSION['user']) {
            return ['error' => setError('Authorization error')];
        }

        $email = $_SESSION['user']['email'];

        $query = "SELECT email,first_name,last_name,city,avatar FROM users WHERE email = ? ";

        $result = databaseExecute($query, $email);

        $response = ['profile_card' => mysqli_fetch_all($result, MYSQLI_ASSOC)];

        return $response;
    }
}