<?php


namespace app\Controllers;


class CartController
{
    public function Index(): array
    {
        if (!$_SESSION['user']) {
            return ['error' => setError('Authorization error')];
        }
        $userId = checker($_SESSION['user']['id'], 'decimal');

        $query = "SELECT DISTINCT p.name, p.price, COUNT(p.id) AS ProductCount, SUM(p.price) AS CommonPrice, p.id FROM products AS p 
                INNER JOIN cart AS c ON c.product_id = p.id WHERE c.user_id = ? GROUP BY p.name;";

        $result = databaseExecute($query, $userId);

        return ['cart_list' => mysqli_fetch_all($result, MYSQLI_ASSOC)];
    }

    public function DeleteItem(array $data = [])
    {
        if (!$_SESSION['user']) {
            return ['error' => setError('Authorization error')];
        }
        $userId = checker($_SESSION['user']['id'], 'decimal');
        $productId = $data['post']['product_id'];

        $query = "DELETE FROM cart WHERE user_id = ? AND product_id = ? LIMIT 1";

        $result = databaseExecute($query, $userId, $productId);

        return $this->Index();
    }
}