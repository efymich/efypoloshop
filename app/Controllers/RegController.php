<?php


namespace app\Controllers;


class RegController
{

    public function Index(): array
    {
        return [];
    }

    public function RegData(array $data = []): array
    {

        $email = checker($data['post']['email'], 'email');
        $first_name = checker($data['post']['first_name'], 'text');
        $last_name = checker($data['post']['last_name'], 'text');
        $city = checker($data['post']['city'], 'text');
        $rawPassword = checker($data['post']['password'], 'password');
        $reRawPassword = checker($data['post']['repassword'], 'password');

        $params = [$email, $first_name, $last_name, $city, $rawPassword, $reRawPassword];

        foreach ($params as $param) {
            if (!$param) {
                return [];
            }
        }

        if (!$email || !$rawPassword || !$reRawPassword) {
            return ['error' => setError('Not exist parameter')];
        }

        if ($rawPassword !== $reRawPassword) {
            return ['error' => setError('Does not match password')];
        }

        $password = password_hash($rawPassword, PASSWORD_DEFAULT);

        $avatarSum = $this->uploadAvatar();


        $query = "INSERT INTO users (email,first_name,last_name,city,password,avatar) VALUES (?,?,?,?,?,?)";

        databaseExecute($query, $email, $first_name, $last_name, $city, $password, $avatarSum);

        if (!databaseErrors()) {
            redirect('login');
        }

        return [];

    }

    private function UploadAvatar(): ?string
    {
        if ((is_uploaded_file($_FILES['avatar']['tmp_name'])) && (substr($_FILES['avatar']['type'], 0, 5) === 'image')) {
            $avatarSum = md5_file($_FILES['avatar']['tmp_name']);
            if (!file_exists("images/avatars/" . $avatarSum)) {
                move_uploaded_file($_FILES['avatar']['tmp_name'], "images/avatars/" . $avatarSum);
            }
            return $avatarSum;
        }
        return null;
    }
}