<?php


namespace app\Controllers;


class LoginController
{
    public function Index(): array
    {
        return [];
    }

    public function Authorization(array $data = []): array
    {
        $email = checker($data['post']['email'], 'email');
        $password = checker($data['post']['password'], 'password');

        $query = "select id,email,password from users where email = ? ";

        $result = databaseExecute($query, $email);

        $user = mysqli_fetch_assoc($result);

        if ($user && password_verify($password, $user['password'])) {
            $_SESSION['user']['id'] = $user['id'];
            $_SESSION['user']['email'] = $user['email'];
            $_SESSION['user']['password'] = $user['password'];
            redirect('index');
            return [];
        }

        return ['error' => setError('Wrong login or password')];
    }

    public function Logout(): array
    {
        if (!isSessionStarted()) {
            return [];
        }

        if (!empty ($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        destroySession();

        redirect('index');
        return [];
    }
}