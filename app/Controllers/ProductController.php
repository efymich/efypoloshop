<?php


namespace app\Controllers;


class ProductController
{
    public function Index(array $data = []): array
    {
        $response = [
            'product_item' => $this->ShowProductData($data),
            'category_list' => $this->ShowCategoryList()
        ];
        return $response;
    }

    public function AddItemToCart(array $data = []): array
    {
        if (!$_SESSION['user']) {
            return ['error' => setError('Authorization error')];
        }

        $email = $_SESSION['user']['email'];
        $productId = $data['post']['product_id'];

        $query = "INSERT INTO cart VALUES ((SELECT u.id FROM users AS u WHERE u.email = ?), 
                                  (SELECT p.id FROM products AS p WHERE p.id = ?))";

        databaseExecute($query, $email, $productId);

        return $this->Index();
    }

    private function ShowProductData(array $data = []): array
    {

        $query = "SELECT p.id,p.name,p.price,p.photo,c.id as categoryId,c.name as categoryName FROM products p LEFT JOIN categories c on c.id = p.category_id ";
        $args = [];
        $i = 0;

        if (!empty($data['get']['name'])) {
            $i += 1;
            $args[$i] = '%' . $data['get']['name'] . '%';
            $query .= isset($args[$i - 1]) ? " AND p.name LIKE ? " : " WHERE p.name LIKE ? ";
        }

        if (!empty($data['get']['category'])) {
            $i += 1;
            $args[$i] = $data['get']['category'];
            $query .= isset($args[$i - 1]) ? " AND c.id = ?" : "WHERE c.id = ? ";
        }

        if (!empty($data['get']['minprice'])) {
            $i += 1;
            $args[$i] = $data['get']['minprice'];
            $query .= isset($args[$i - 1]) ? " AND p.price >= ?" : "WHERE p.price >= ? ";
        }

        if (!empty($data['get']['maxprice'])) {
            $i += 1;
            $args[$i] = $data['get']['maxprice'];
            $query .= isset($args[$i - 1]) ? " AND p.price <= ?" : "WHERE p.price <= ? ";
        }

        $result = databaseExecute($query, ...$args);

        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    private function ShowCategoryList(): array
    {
        $query = "SELECT id as categoryId ,name as categoryName FROM categories";

        $result = databaseExecute($query);

        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}