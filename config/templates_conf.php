<?php
return [
    "dir" => "../templates/",
    "pages" => [
        'index.html' => [
            'title' => 'Main page',
            'template' => 'index.html',
            'name' => 'Main'
        ],
        'products' => [
            'title' => 'Products',
            'template' => 'products.html',
            'name' => 'Products'
        ],
        'profile' => [
            'title' => 'Profile',
            'template' => 'profile.html',
            'name' => 'Profile'
        ],
        'login' => [
            'title' => 'Login',
            'template' => 'login.html',
            'name' => 'Login'
        ],
        'reg' => [
            'title' => 'Registration',
            'template' => 'reg.html',
            'name' => 'Registration'
        ],
        'cart' => [
            'title' => 'Cart',
            'template' => 'cart.html',
            'name' => 'Cart'
        ]


    ],
    "default" => "index.html",
    "auth" => ['reg', 'login'],
    "pages_appear" => [
        'profile' => 'Profile',
        'cart' => 'Cart',
        'logout' => 'LogOut'
    ],
];