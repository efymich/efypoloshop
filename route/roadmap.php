<?php

use core\Router\Route;

// Main actions

Route::Get("/", app\Controllers\MainController::class, 'Index');

Route::Get("/index", app\Controllers\MainController::class, 'Index');

// Product actions

Route::Get("/products", app\Controllers\ProductController::class, 'Index');

Route::Post("/products", app\Controllers\ProductController::class, 'AddItemToCart');

// Profile actions

Route::Get("/profile", app\Controllers\ProfileController::class, 'ShowData');

// Cart actions

Route::Get("/cart", app\Controllers\CartController::class, 'Index');

Route::Post("/cart", app\Controllers\CartController::class, 'DeleteItem');

// Reg actions

Route::Get("/reg", app\Controllers\RegController::class, 'Index');

Route::Post("/reg", app\Controllers\RegController::class, 'RegData');

// Login actions

Route::Get("/login", app\Controllers\LoginController::class, 'Index');

Route::Get("/logout", app\Controllers\LoginController::class, 'Logout');

Route::Post("/login", app\Controllers\LoginController::class, 'Authorization');

