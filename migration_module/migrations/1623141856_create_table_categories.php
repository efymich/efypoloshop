    <?php
    
    $pull = "CREATE TABLE categories (id serial PRIMARY KEY , name text)";
    
    $rollback = "DROP TABLE categories";
    
    return [
        'pull' => $pull,
        'rollback' => $rollback
        ];