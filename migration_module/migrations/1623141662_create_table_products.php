    <?php
    
    $pull = "CREATE TABLE products (id serial PRIMARY KEY ,
                                    name text,
                                    price decimal,
                                    photo varchar(255),
                                    category_id bigint unsigned NOT NULL)";
    
    $rollback = "DROP TABLE products";
    
    return [
        'pull' => $pull,
        'rollback' => $rollback
        ];