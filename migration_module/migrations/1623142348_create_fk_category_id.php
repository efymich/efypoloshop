    <?php
    
    $pull = "ALTER TABLE products ADD CONSTRAINT FOREIGN KEY (category_id) REFERENCES categories(id)";
    
    $rollback = "";
    
    return [
        'pull' => $pull,
        'rollback' => $rollback
        ];