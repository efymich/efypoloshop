    <?php
    
    $pull = "CREATE TABLE users (id serial PRIMARY KEY ,
                                    email varchar(50) UNIQUE NOT NULL ,
                                    first_name text NOT NULL,
                                    last_name text NOT NULL,
                                    city text NOT NULL,
                                    password varchar(255) NOT NULL,
                                    avatar varchar(32),
                                    CHECK ( city IN ('Moscow','Saint-Petersburg','Novgorod','Yaroslavl')))";
    
    $rollback = "DROP TABLE users";
    
    return [
        'pull' => $pull,
        'rollback' => $rollback
        ];