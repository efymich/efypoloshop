    <?php
    
    $pull = "CREATE TABLE cart (user_id bigint UNSIGNED NOT NULL,
                                product_id  bigint UNSIGNED NOT NULL ,
                                    CONSTRAINT FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
                                    CONSTRAINT FOREIGN KEY (product_id) REFERENCES products(id) ON DELETE CASCADE )";
    
    $rollback = "DROP TABLE cart";
    
    return [
        'pull' => $pull,
        'rollback' => $rollback
        ];