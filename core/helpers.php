<?php
function redirect(string $page): void
{
    $config = require "../config/endpoint_conf.php";
    $location = $config['location'];
    header("Location: http://$location/" . $page);
    die();
}

function setError (string $error = '' ): string {
    static $errorInstance = null;

    if ($errorInstance){
        return $errorInstance;
    }

    $errorInstance = $error;
    return $errorInstance;
}

function checker (string $value,string $type): ?string {
    switch ($type) {
        case 'email':
            $value = htmlspecialchars(trim($value),ENT_DISALLOWED);
            if (!preg_match("/^\w+@+\w+\.\w{1,5}$/",$value)) {
                setError('Incorrect email');
                $value = null;
            }
            break;
        case 'password':
            $value = htmlspecialchars(trim($value),ENT_DISALLOWED);
            if (!preg_match("/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,20}$/",$value)) {
                setError('Incorrect password');
                $value = null;
            }
            break;
        case 'text':
            $value = htmlspecialchars(trim($value),ENT_DISALLOWED);
            if (!preg_match("/^[a-zA-Z]+([-]*)+[a-zA-Z]{2,20}$/",$value)) {
                setError('Incorrect text');
                $value = null;
            }
            break;
        case 'decimal':
            $value = htmlspecialchars(trim($value),ENT_DISALLOWED);
            if (!preg_match("/^[ 0-9]+$/",$value)) {
                $value = null;
            }
            break;
    }

    return $value;
}
