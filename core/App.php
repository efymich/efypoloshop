<?php


namespace core;

use core\Router\RouteContainer;
use Twig\Environment;
use Twig\Extension\DebugExtension;
use Twig\Loader\FilesystemLoader;

require "database.php";
require "parser.php";
require "session.php";
require "helpers.php";

function appMode(): void
{
    $config = require "../config/app_conf.php";

    if (!empty($config['mode']) && $config['mode'] === 'debug') {
        ini_set('error_reporting', E_ALL);
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        echo "<b>Debug mode is on!</b>";
    }
}

appMode();

class App
{
    public static string $page = 'index';

    public static function Init()
    {
        require '../route/roadmap.php';
        $url = self::clearUrlFromGet();
        $type = strtolower($_SERVER['REQUEST_METHOD']);
        $controllerRes = RouteContainer::Searcher($url, $type, array_merge(self::prepareGetArray(), self::preparePostArray()));
        if (preg_match('/\/(\w+)(|\/)/', $url, $matches)) {
            self::$page = $matches[1];
        }
        self::Run(self::$page, $controllerRes);
    }

    private static function Run(string $page, array $data = [])
    {
        databaseClose();
        $loader = new FilesystemLoader('../templates');
        $twig = new Environment($loader, ['debug' => true]);
        $twig->addExtension(new DebugExtension());
        $twig->addGlobal('session', $_SESSION);
        $template = $page . ".html";
        echo $twig->render($template, $data);
//        parser($page, $data);
    }

    private static function prepareGetArray(): array
    {
        $data = [];
        foreach ($_GET as $key => $value) {
            $data['get'][$key] = $value;
        }
        return $data;
    }

    private static function preparePostArray(): array
    {
        $data = [];
        foreach ($_POST as $key => $value) {
            $data['post'][$key] = $value;
        }
        return $data;
    }


    private static function clearUrlFromGet(): string
    {
        $serverUrl = $_SERVER['REQUEST_URI'];
        if (empty($_SERVER['QUERY_STRING'])) {
            return $serverUrl;
        }
        $clearServerUrl = substr_replace($serverUrl, '', strpos($serverUrl, '?' . $_SERVER['QUERY_STRING']));
        return $clearServerUrl;
    }
}