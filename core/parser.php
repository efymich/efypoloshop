<?php

function parser(string $page, array $response = [])
{

// <Вызов страницы по полученному GET параметру>
    $config = require "../config/templates_conf.php";

    $pages = $config['pages'];
    $curPage = $pages[$config['default']];

    if (!empty($pages[$page])) {
        $curPage = $pages[$page];
    }
    $content = file_get_contents($config['dir'] . $curPage['template']);

    if (!$content) {
        die($content);
    }

    $menuList = parseMenu($pages, $config);
    $time = giveDate();
    $header = parseHeader(['app_menu', 'title'], [$menuList, $curPage['title']], $config);
    $footer = parseFooter($config, ['time'], [$time]);


    $content = parseAppInclude(['header', 'footer'], [$header, $footer], $content);
    $content = showError(setError(), $content);
    die(parseResponse($response, $content, $config));
}


function parseResponse(array $response, string $content, array $config): string
{
    $template = '';
    foreach ($response as $key => $data) {
        foreach ($data as $val) {
            $template .= parseTemplate($key, $config);
            foreach ($val as $hook => $item) {
                if ($hook === 'avatar' && $item === null) {
                    $item = 'default_avatar.png';
                }
                $template = str_replace("{{ " . strtoupper($hook) . " }}", $item, $template);
            }
        }
        $content = str_replace("{{ " . strtoupper($key) . " }}", $template, $content);

    }
    return $content;
}


function parseTemplate(string $tempName, array $config): string
{
    return file_get_contents($config['dir'] . $tempName . '.html');
}

function parseMenu(array $pages, array $config)
{
    $menuList = '';

    foreach ($pages as $pageName => $val) {
        if (isSessionStarted() && !empty($_SESSION['user']) && in_array($pageName, $config['auth'], true)) {
            continue;
        }
        if ($pageName === 'profile' || $pageName === 'cart') {
            continue;
        }
        $menuList .= file_get_contents($config['dir'] . 'page_link.html');

        $menuList = str_replace(
            [
                '{{ CLASS }}',
                '{{ PAGE }}',
                '{{ NAME }}'
            ],
            [
                'class = "my_menu"',
                $pageName,
                $val['name']
            ],
            $menuList
        );
    }

    $pages_appear = $config['pages_appear'];
    foreach ($pages_appear as $pageName => $val) {
        if (isSessionStarted() && !empty($_SESSION['user'])) {
            $menuList .= file_get_contents($config['dir'] . "page_link.html");

            $menuList = str_replace(
                [
                    '{{ CLASS }}',
                    '{{ PAGE }}',
                    '{{ NAME }}'
                ],
                [
                    'class = "my_menu"',
                    $pageName,
                    $val,
                ],
                $menuList
            );
        }
    }
    return $menuList;
}

function parseHeader(array $keys, array $data, array $config): string
{

    $template = file_get_contents($config['dir'] . 'header.html');

    foreach ($keys as $index => $val) {
        $val = strtoupper($val);

        $template = str_replace("{{ $val }}", $data[$index], $template);
    }

    return $template;
}

function giveDate()
{
    return date('d.m.Y');
}


function parseFooter(array $config, array $keys = [], array $data = []): string
{

    $template = file_get_contents($config['dir'] . 'footer.html');

    foreach ($keys as $index => $val) {
        $val = strtoupper($val);

        $template = str_replace("{{ $val }}", $data[$index], $template);
    }

    return $template;
}

function parseAppInclude(array $keys, array $data, string $content): string
{

    foreach ($keys as $index => $val) {
        $val = strtoupper($val);

        $content = str_replace("{{ $val }}", $data[$index], $content);
    }

    return $content;

}

function showError(string $error, $content)
{
    return str_replace('{{ ERROR }}', $error, $content);
}
