<?php
function isSessionStarted()
{
    $status = session_status() === PHP_SESSION_ACTIVE ? true : false;
    
    $sesId = session_id() === '' ? false : true;

    return ($status || $sesId);
}
if ( isSessionStarted() === false ) session_start();

function destroySession ()
{
    if (session_status() === PHP_SESSION_ACTIVE) {
        session_destroy();
    }
}
